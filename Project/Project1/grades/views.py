# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import unicode_literals
from rest_framework.views import APIView
from .models import Grade
from .serializers import GradesSerializer
from rest_framework.response import Response
from rest_framework import status
from student import Profile
from courses import Course


from django.shortcuts import render

# Create your views here.
class GradesView(APIView):
    def get(self,request):
        grades = Grade.objects.all()
        serializer = GradesSerializer(grades, many=True)
        return Response(serializer.data)

    def post(self,request):
        serializer = GradesSerializer(data=request.data)
        pk1=serializer.validated_data["student_id"]
        pk2 = serializer.validated_data["course_id"]
        student=self.get_student(pk1)
        course=self.get_course(pk2)
        #there is no Error checking b2a :'D
        if serializer.is_valid():
            course = Grade.objects.create(grade=serializer.validated_data["grade"],
                                          student=student,
                                          course=course,
                                          )
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_student(self,pk):
        student = Profile.objects.get(id=pk)
        return student


    def get_course(selfs,pk):
        course = Profile.objects.get(id=pk)
        return course

