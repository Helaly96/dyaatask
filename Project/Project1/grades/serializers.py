from rest_framework import serializers
from .models import Grades
from courses.serializers import CourseSerializer
from student.serializers import ProfileSerializer



class GradesSerializer(serializers.ModelSerializer):
    student = ProfileSerializer(many=True)
    course=CourseSerializer(many=True)
    class Meta:
      model=Course
      fields='__all__'