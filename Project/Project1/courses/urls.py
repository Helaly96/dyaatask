from courses import views
from django.conf.urls import url

urlpatterns = [
    url(r'^$', views.CourseView.as_view()),
    url(r'CourseSpecific^$', views.SpecificCourseView.as_view()),

]

