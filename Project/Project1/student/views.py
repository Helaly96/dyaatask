# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework.views import APIView
from .models import Profile
from .serializers import ProfileSerializer
from rest_framework.response import Response
from rest_framework import status


# Create your views here.
class ProfileView(APIView):

    def get(self,request):
        students = Profile.objects.all()
        serializer = ProfileSerializer(students, many=True)
        return Response(serializer.data)


    def post(self,request):
        serializer = ProfileSerializer(data=request.data)
        #there is no Error checking b2a :'D
        if serializer.is_valid():
            user = Profile.objects.create(firstname=serializer.validated_data["firstname"],
                                            lastname=serializer.validated_data["lastname"],
                                            year=serializer.validated_data["year"])
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    def update(self,request):
        serializer = ProfileSerializer(data=request.data)
        id=serializer.validated_data['id']
        GoodStudent=self.get_specific_student(id)
        GoodStudent.firstname=serializer.validated_data['firstname']
        GoodStudent.lastName = serializer.validated_data['lastName']
        GoodStudent.year = serializer.validated_data['year']
        return Response ({'message':"updated"},status=status.HTTP_201_CREATED)


    def get_specific_student(self,pk):
        student=Profile.objects.get(id=pk)
        return  student

    def delete(self,request):
        serializer = ProfileSerializer(data=request.data)
        pk=serializer.validated_data['id']
        badstudnet=self.get_specific_student(pk)
        badstudnet.delete()
        return Response ({'message':"deleted"},status=status.HTTP_201_CREATED)












